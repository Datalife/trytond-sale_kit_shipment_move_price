=====================================
Sale Kit Shipment Move Price Scenario
=====================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from decimal import Decimal

Install sale_kit_shipment_move_price::

    >>> config = activate_modules(['sale_kit_shipment_move_price', 'stock_valued'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts()
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create account category::

    >>> Category = Model.get('product.category')

    >>> account_category = Category()
    >>> account_category.name = 'Account category'
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> account_category.save()

Create products with explode_kit_in_sales::

    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')
    >>> KitLine = Model.get('product.kit.line')

    >>> unit, = Uom.find([('name', '=', 'Unit')])

    >>> template = Template()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('11')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products

    >>> template2 = Template()
    >>> template2.name = 'Product 2'
    >>> template2.default_uom = unit
    >>> template2.type = 'goods'
    >>> template2.salable = True
    >>> template2.list_price = Decimal('15')
    >>> template2.cost_price_method = 'fixed'
    >>> template2.account_category = account_category
    >>> template2.save()
    >>> product2, = template2.products

    >>> template3 = Template()
    >>> template3.name = 'Product 3'
    >>> template3.default_uom = unit
    >>> template3.type = 'goods'
    >>> template3.salable = True
    >>> template3.list_price = Decimal('12')
    >>> template3.cost_price_method = 'fixed'
    >>> template3.account_category = account_category
    >>> template3.save()
    >>> product3, = template3.products

    >>> template4 = Template()
    >>> template4.name = 'Service'
    >>> template4.default_uom = unit
    >>> template4.type = 'service'
    >>> template4.salable = True
    >>> template4.list_price = Decimal('120')
    >>> template4.cost_price_method = 'fixed'
    >>> template4.account_category = account_category
    >>> template4.save()

    >>> service, = template4.products
    >>> service.kit = True
    >>> service.explode_kit_in_sales = True
    >>> service.save()

    >>> kit_line = service.kit_lines.new()
    >>> kit_line.product = product
    >>> kit_line.quantity = 6
    >>> kit_line2 = service.kit_lines.new()
    >>> kit_line2.product = product2
    >>> kit_line2.quantity = 2
    >>> kit_line3 = service.kit_lines.new()
    >>> kit_line3.product = product3
    >>> kit_line3.quantity = 2
    >>> service.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Sale product::

    >>> Sale = Model.get('sale.sale')

    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = service
    >>> sale_line.quantity = 1
    >>> sale_line.unit_price
    Decimal('120.0000')
    >>> sale.save()
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')

Check shipment moves unit price::

    >>> ShipmentOut = Model.get('stock.shipment.out')

    >>> shipment, = ShipmentOut.find([])
    >>> moves_explode = [move for move in shipment.outgoing_moves
    ...     if move.product in [product, product2, product3]]
    >>> [move.unit_price for move in moves_explode if move.product == product][0]
    Decimal('11.0000')
    >>> [move.unit_price for move in moves_explode if move.product == product2][0]
    Decimal('15.0000')
    >>> [move.unit_price for move in moves_explode if move.product == product3][0]
    Decimal('12.0000')

Sale again with different price::

    >>> Sale = Model.get('sale.sale')

    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = service
    >>> sale_line.quantity = 1
    >>> sale_line.unit_price = Decimal('100')
    >>> sale.save()
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> shipment, = sale.shipments
    >>> moves_explode = [move for move in shipment.outgoing_moves
    ...     if move.product in [product, product2, product3]]
    >>> [move.unit_price for move in moves_explode if move.product == product][0]
    Decimal('9.1667')
    >>> [move.unit_price for move in moves_explode if move.product == product2][0]
    Decimal('12.5000')
    >>> [move.unit_price for move in moves_explode if move.product == product3][0]
    Decimal('10.0000')

Create new kit::

    >>> template5 = Template()
    >>> template5.name = 'Service'
    >>> template5.default_uom = unit
    >>> template5.type = 'service'
    >>> template5.salable = True
    >>> template5.list_price = Decimal('30')
    >>> template5.cost_price_method = 'fixed'
    >>> template5.account_category = account_category
    >>> template5.save()

    >>> service, = template5.products
    >>> service.kit = True
    >>> service.explode_kit_in_sales = True
    >>> service.save()

    >>> kit_line = service.kit_lines.new()
    >>> kit_line.product = product
    >>> kit_line.quantity = 1
    >>> kit_line2 = service.kit_lines.new()
    >>> kit_line2.product = product2
    >>> kit_line2.quantity = 1
    >>> kit_line3 = service.kit_lines.new()
    >>> kit_line3.product = product3
    >>> kit_line3.quantity = 1
    >>> service.save()

    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = service
    >>> sale_line.quantity = 1
    >>> sale.save()
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> shipment, = sale.shipments
    >>> moves_explode = [move for move in shipment.outgoing_moves
    ...     if move.product in [product, product2, product3]]
    >>> [move.unit_price for move in moves_explode if move.product == product][0]
    Decimal('8.6842')
    >>> [move.unit_price for move in moves_explode if move.product == product2][0]
    Decimal('11.8421')
    >>> [move.unit_price for move in moves_explode if move.product == product3][0]
    Decimal('9.4737')
    >>> moves_explode = [move for move in shipment.inventory_moves
    ...     if move.product in [product, product2, product3]]
    >>> [move.unit_price for move in moves_explode if move.product == product][0]
    Decimal('8.6842')
    >>> [move.unit_price for move in moves_explode if move.product == product2][0]
    Decimal('11.8421')
    >>> [move.unit_price for move in moves_explode if move.product == product3][0]
    Decimal('9.4737')